document.getElementById("hobbyform").addEventListener("submit", onSubmit);

function onSubmit(e) {
  e.preventDefault();
  $.post(`/addhobby`,
          {
            hobby:$("#nme").val(),
          },
          function(data, status){
          $('.message').empty();
          $("#hobbies").prepend(`<div class="alert alert-success">${$("#nme").val()}</div>`);
          $("#nme").val(' ');
          setTimeout(function(){alert('check your mail for notification!');},3000)
          });
          $('.message').empty();
          $('.message').prepend(`<div class="alert alert-info">
    Your request is being processed...
  </div>`);
    return false;
}
