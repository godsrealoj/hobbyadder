require('dotenv').config();
var express = require('express');
var mongoose = require('mongoose');
var bodyParser = require('body-parser');
var urlencodedParser = bodyParser.urlencoded({ extended: true });
var app = express();
app.set('view engine','ejs');
var session = require('express-session');
var MongoStore = require('connect-mongo')(session);
var sessionMiddleware = session({
  secret: process.env.MYSECRET,
  resave: false,
  saveUninitialized: true,
  store:new MongoStore({mongooseConnection: mongoose.connection, ttl: 4*60*60})
});
const port = process.env.port || 3000;
app.use(sessionMiddleware);
app.use('/public',express.static('public'));
app.use(urlencodedParser);
//app.use(bodyParser.json());
mongoose.Promise = global.Promise;
mongoose.connect(process.env.CONNECTIONURI,{
  socketTimeoutMS: 0,
  keepAlive: true,
  reconnectTries:  Number.MAX_VALUE
}).then(
  () => {},
).catch(err => {console.log(err)});
var User = require('./models/user');
app.get('/', function(req,res){
  res.render('home');
});
app.get('/dash', function(req,res){
  res.render('dashboard');
});
app.post('/register', function(req, res){
User.findOne({username:req.body.username}, function(err,user){
  if(err) return err;
  if(user){res.render('signup',{message:'user already exists'});return}
  require('./helpers/createuser.js')(req.body,req,res);
});
});
app.post('/login', function(req, res){
  User.findOne({email:req.body.email}, function(err,user){
    if(err) return err;
    if(!user){res.render('login',{message:'Invalid username or password'});return}
    if(user.password !== req.body.password){res.render('login',{message:'Invalid username or password'});return}
    req.session.user = user;
    res.redirect('/dashboard');
  });
});
app.post('/addhobby', function(req, res){
  if(!req.session.user){res.redirect('/login');return;}
  User.findOne({username:req.session.user.username}, function(err,user){
    user.hobbies.push(req.body.hobby);
    user.save();
    res.json(`Your hobby has been added, you would be updated right away.`);
    req.session.user = user;
  });
  require('./helpers/notify.js')(req.session.user.email,req.session.user.username,req.body.hobby);
});

app.get('/login', function(req,res){
  if(req.session.user){res.redirect('/dashboard');return;}
  res.render('login',{message:null});
});

app.get('/register', function(req,res){
  if(req.session.user){res.redirect('/dashboard');return;}
  res.render('signup',{message:null});
});

app.get('/dashboard', function(req,res){
  if(!req.session.user){res.redirect('/login');return;}
  User.findOne({username:req.session.user.username}, function(err,user){
    req.session.user=user;
    res.render('dashboard',{username:req.session.user.username,hobbies:req.session.user.hobbies});
  });
});

app.get('/logout',function(req,res){
  if(!req.session.user){res.redirect('/login');return;}
   req.session.destroy(function(err){
     if (err) console.log(err);
  res.redirect('/login');
  });
});

app.listen(port,function(err){
  if(err) throw err;
  console.log("Listening on port 3000");
});
