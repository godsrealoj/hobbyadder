var mongoose = require('mongoose');
//var bcrypt = require('bcrypt-nodejs');
var Schema = mongoose.Schema;
// CREATE USER SCHEMA
var UserSchema = new Schema({
  username:{type: String,unique: true},
  email:{type: String,lowercase: true},
  password:String,
  number: String,
  hobbies: []

},{collection:'Users'});

module.exports = mongoose.model('User',UserSchema);
